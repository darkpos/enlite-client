//

module.exports = class Session {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  validate() {
    return this.enlite.validateToken();
  }

  //
  async login(email, password) {
    if (!email)
      throw new Error('Email field is empty');

    this.enlite.setToken(null);
    
    const response = await this.enlite.request.post(`session/login`, { email, password });

    if (response.data.status && response.data.status === 'error')
      throw new Error(response.data.message || 'Login failed.')

    this.enlite.setToken(response.data.token);
    return response;
  }

  //
  async emailAuth(email, notify, store_id) {
    if (!email)
      throw new Error('Email field is empty');

    const response = await this.enlite.request.post(`session/emailAuth`, { email, notify, store_id });
    return response;
  }

  //
  async verifyCode(email, code, store_id) {
    if (!email)
      throw new Error('Email is empty');
    if (!code)
      throw new Error('Code is empty');

    const response = await this.enlite.request.post('session/verifyCode', { email, code });

    if (response.data && response.data.length) {
      const store = (response.data.length === 1) ?
        response.data[0] :
        response.data.find(s => s.store_id === store_id);

      if (store && store.token) {
        this.enlite.setToken(store.token);
        response.data = [store];
      }
    }

    return response;
  }

}