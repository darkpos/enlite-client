//
module.exports = class Ticket {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async getTicket(store_id, ticket_id) {
    if (!ticket_id || !store_id)
      throw new Error('Store ID and Ticket ID are required');

    const response = await this.enlite.request.get(`ticket/${ticket_id}`, { params: { store_id } });
    return response;
  }

  //
  async getTickets(store_id, query) {
    if (!store_id)
      throw new Error('Store ID is required');

    const response = await this.enlite.request.get(`ticket`, { params: { store_id, ...query } });
    return response;
  }

  //
  async getTicketPreview(store_id, ticket_id, template_id, api_token) {
    if (!ticket_id || !store_id || !template_id || !api_token)
      throw new Error('Store ID, Ticket ID, Template ID and API Token are required');

    const q = new Buffer.from(JSON.stringify({
      apiToken: api_token,
      storeId: store_id,
      templateId: template_id
    })).toString('hex');

    const response = await this.enlite.request.get(`ticket/preview/${ticket_id}`, { params: { q } });
    return response;
  }

  //
  async createQuickTicket(store_id, ticket) {
    if (!store_id || !ticket)
      throw new Error('Store ID or Ticket is missing');

    const response = await this.enlite.request.post('ticket/quick', { store_id, ticket });
    return response;
  }

  //
  async addTicketNote(store_id, ticket_id, notes, updated_employee_id) {
    if (!store_id || !ticket_id || !notes || !updated_employee_id)
      throw new Error('Store ID, Ticket ID, Notes and Updated Employee ID are required');

    const response = await this.enlite.request.put(`ticket/note/${ticket_id}`, { store_id, notes, updated_employee_id });
    return response;
  }

  //
  async void(store_id, ticket_id, notes, employee_id) {
    if (!store_id || !ticket_id || !employee_id)
      throw new Error('Store ID, Ticket ID and Employee ID are required');

    const response = await this.enlite.request.put(`ticket/void/${ticket_id}`, { store_id, notes, employee_id });
    return response;
  }

  //
  async getInvoiceStatus(firm_id, store_id) {
    return await this.enlite.invoice.getInvoiceStatus(firm_id, store_id);
  }

  //
  async updateInvoiceStatus(firm_id, store_id, invoice_id, status_id) {
    return await this.enlite.invoice.updateInvoiceStatus(firm_id, store_id, invoice_id, status_id);
  }

  //
  async addInvoiceNote(store_id, invoice_id, notes, updated_employee_id) {
    return await this.enlite.invoice.addInvoiceNote(store_id, invoice_id, notes, updated_employee_id);
  }

}