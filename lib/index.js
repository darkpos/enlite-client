//
const axios = require('axios');
const jwtDecode = require('jwt-decode');
const { nanoid } = require('nanoid');
const { version } = require('../package.json');

module.exports = class Enlite {
  constructor({ url, app_name, app_version, hardware_id }) {
    this.agent = [`EnliteClient/${version}`]
    this.baseUrl = url

    this.request = axios.create({
      baseURL: this.baseUrl
    })

    // headers
    
    this.request.defaults.headers.common['Content-Type'] = 'application/json'
    this.request.defaults.headers.common['Accept'] = 'application/json'
    this.request.defaults.headers.common['x-darkpos-enlite-session'] = nanoid()
    if (app_name && app_version) {
      this.agent.push(`${app_name}/${app_version}`)
    }
    this.request.defaults.headers.common['x-darkpos-enlite-agent'] = this.agent.join(' ')
    if (hardware_id) {
      this.request.defaults.headers.common['x-darkpos-hardware-id'] = `${hardware_id}`
    }

    this.service = new (require('./service'))(this);
    this.session = new (require('./session'))(this);
    this.store = new (require('./store'))(this);
    this.customer = new (require('./customer'))(this);
    this.ticket = new (require('./ticket'))(this);
    this.invoice = new (require('./invoice'))(this);
    this.payment = new (require('./payment'))(this);
    this.menu = new (require('./menu'))(this);
    this.user = new (require('./user'))(this);

    return this
  }

  setToken(token) {
    this.request.defaults.headers.common['Authorization'] = token
  }

  getToken() {
    return this.request.defaults.headers.common['Authorization']
  }

  validateToken(token, set) {
    try {
      const jwt = jwtDecode(token || this.request.defaults.headers.common['Authorization'])
      const now = Date.now()
      if (now < (jwt.exp*1000)) {
        if (token && set === true) this.setToken(token)
        return jwt
      }
    } catch (err) {}
    return null
  }
}