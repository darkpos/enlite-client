//
module.exports = class Payment {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async getPayment(store_id, payment_id) {
    if (!payment_id || !store_id)
      throw new Error('Store ID and Payment ID are required');

    const response = await this.enlite.request.get(`payment/${payment_id}`, { params: { store_id } });
    return response;
  }

  //
  async getPayments(store_id, query) {
    if (!store_id)
      throw new Error('Store ID is required');

    const response = await this.enlite.request.get(`payment`, { params: { store_id, ...query } });
    return response;
  }

  //
  async authorize(method, data) {
    if (!method)
      throw new Error('Payment method is required');

    const response = await this.enlite.request.post(`payment/authorize`, { method, ...data });
    return response;
  }

}