//
module.exports = class Store {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async getAllStores(firm_id) {
    if (!firm_id)
      throw new Error('Firm ID is missing');

    const response = await this.enlite.request.get(`store/all`, { params: { firm_id } });
    return response;
  }

  //
  async getLocalStores(lat, lon, distance=25) {
    if (!lat || !lon)
      throw new Error('Latitute or Longitude is missing');

    const response = await this.enlite.request.get(`store/local`, { params: { lat, lon, distance } });
    return response;
  }

  //
  async getPublicStore(store_id) {
    if (!store_id)
      throw new Error('Store ID is missing');

    const response = await this.enlite.request.get(`store/${store_id}`);

    if (response.data) {
      const store = response.data;

      if (store && store.token) {
        this.enlite.setToken(store.token);
        response.data = [store];
      }
    }

    return response;
  }

  //
  async getStore(firm_id, store_id) {
    if (!firm_id || !store_id)
      throw new Error('Firm ID or Store ID is missing');

    const response = await this.enlite.request.get(`store/${firm_id}/${store_id}`);
    return response;
  }

  //
  async getBusySlots(firm_id, store_id, from_date, to_date) {
    if (!firm_id || !store_id)
      throw new Error('Firm ID and Store ID are required');

    const response = await this.enlite.request.get(`store/busySlots/${firm_id}/${store_id}`, { params: { from_date, to_date } });
    return response;
  }

  //
  async getStoreRoute(firm_id, store_id, route_id, updated_employee_id, action) {
    if (!firm_id || !store_id || !route_id)
      throw new Error('Firm ID, Store ID and Route ID are required');

    const response = await this.enlite.request.get(`store/${firm_id}/${store_id}/route/${route_id}`, { params: { updated_employee_id, action } });
    return response;
  }

  //
  async getStoreRacks(firm_id, store_id, name) {
    if (!store_id)
      throw new Error('Store ID is required');

    const response = await this.enlite.request.get(`store/racks/${firm_id}/${store_id}`, { params: { name } });
    return response;
  }

  //
  async sendFeedback(store_id, user_id, subject, message, type, from_email, from_user_id) {
    if (!store_id || !user_id)
      throw new Error('Store ID or User ID is missing');

    const response = await this.enlite.request.put('store/feedback', { store_id, user_id, subject, message, type, from_email, from_user_id });
    return response;
  }

  //
  async uploadImage(store_id, base64) {
    if (!store_id || !base64)
      throw new Error('Store ID or Base64 is missing');

    const response = await this.enlite.request.put('store/image', { store_id, base64 });
    return response;
  }

}