//
module.exports = class User {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async timecards(store_id, user_id) {
    if (!store_id || !user_id)
      throw new Error('Store ID or User ID is missing');

    const response = await this.enlite.request.get('user/timecard', { params: { store_id, user_id } });
    return response;
  }

  //
  async timecardOpen(store_id, user_id) {
    if (!store_id || !user_id)
      throw new Error('Store ID or User ID is missing');

    const response = await this.enlite.request.post('user/timecard/open', { store_id, user_id });
    return response;
  }

  //
  async timecardClose(store_id, timecard_id, note) {
    if (!store_id || !timecard_id)
      throw new Error('Store ID or Timecard ID is missing');

      const response = await this.enlite.request.post('user/timecard/close', { store_id, timecard_id, note });
    return response;
  }
}