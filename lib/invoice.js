//
module.exports = class Invoice {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async batch(store_id, employee_id, putInvoices, pickInvoices, voidInvoices) {
    if (!store_id || !employee_id)
      throw new Error('Store ID and Employee ID are required');

    const puts = Array.isArray(putInvoices) ? putInvoices : null;
    const picks = Array.isArray(pickInvoices) ? pickInvoices : null;
    const voids = Array.isArray(voidInvoices) ? voidInvoices : null;

    const response = await this.enlite.request.put(`invoice/batch`, { store_id, employee_id, puts, picks, voids });
    return response;
  }

  //
  async getInvoice(store_id, invoice_id) {
    if (!invoice_id || !store_id)
      throw new Error('Store ID and Invoice ID are required');

    const response = await this.enlite.request.get(`invoice/${invoice_id}`, { params: { store_id } });
    return response;
  }

  //
  async putInvoice(store_id, invoice_id, invoice, updated_employee_id) {
    if (!invoice_id || !invoice || !updated_employee_id || !store_id)
      throw new Error('Store ID, Invoice ID and Invoice are required');

    const response = await this.enlite.request.put(`invoice/${invoice_id}`, { invoice, store_id, updated_employee_id });
    return response;
  }

  //
  async getInvoices(store_id, query) {
    if (!store_id)
      throw new Error('Store ID is required');

    const response = await this.enlite.request.get(`invoice`, { params: { store_id, ...query } });
    return response;
  }

  //
  async getInvoicePreview(store_id, invoice_id, template_id, api_token) {
    if (!invoice_id || !store_id || !template_id || !api_token)
      throw new Error('Store ID, Invoice ID, Template ID and API Token are required');

    const q = new Buffer.from(JSON.stringify({
      apiToken: api_token,
      storeId: store_id,
      templateId: template_id
    })).toString('hex');
    
    const response = await this.enlite.request.get(`invoice/preview/${invoice_id}`, { params: { q } });
    return response;
  }

  //
  async getInvoiceStatus(firm_id, store_id) {
    if (!firm_id || !store_id)
      throw new Error('Firm ID and Store ID are required');

    const response = await this.enlite.request.get(`invoice/status/${firm_id}/${store_id}`);
    return response;
  }

  //
  async updateInvoiceStatus(firm_id, store_id, invoice_id, status_id) {
    if (!firm_id || !store_id || !invoice_id)
      throw new Error('Firm ID, Store ID and Invoice ID are required');

    const response = await this.enlite.request.put(`invoice/status/${invoice_id}`, { store_id, firm_id, status_id });
    return response;
  }

  //
  async addInvoiceNote(store_id, invoice_id, notes, updated_employee_id) {
    if (!store_id || !invoice_id || !notes || !updated_employee_id)
      throw new Error('Store ID, Invoice ID, Notes and Updated Employee ID are required');

    const response = await this.enlite.request.put(`invoice/note/${invoice_id}`, { store_id, notes, updated_employee_id });
    return response;
  }

  //
  async void(store_id, invoice_id, notes, employee_id) {
    if (!store_id || !invoice_id || !employee_id)
      throw new Error('Store ID, Invoice ID and Employee ID are required');

    const response = await this.enlite.request.put(`invoice/void/${invoice_id}`, { store_id, notes, employee_id });
    return response;
  }

  //
  async invoicePicked(store_id, invoice_id_array, employee_id) {
    if (!store_id || !invoice_id_array || !employee_id)
      throw new Error('Store ID, Invoice IDs, Employee ID are required');

    const response = await this.enlite.request.post(`invoice/picked`, { store_id, invoice_id_array, employee_id });
    return response;
  }

  //
  async invoicePaid(store_id, customer_id, pay_ticket_id, pay_invoice_item_id, total_paid, transaction_number, card_token, payment_settings) {
    if (!store_id || !customer_id || !pay_ticket_id || !pay_invoice_item_id || !total_paid || !payment_settings)
      throw new Error('Store ID, Customer ID, Payment Settings and Payment Info are required');

    if (!payment_settings.payment_type_code)
        throw new Error('Payment Type Code is missing');

    const response = await this.enlite.request.post(`invoice/paid`, { store_id, payment_settings, customer_id, pay_invoice_item_id, pay_ticket_id, total_paid, transaction_number, card_token });
    return response;
  }

}