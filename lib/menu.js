//
module.exports = class Menu {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async purge(key, store_id, purgeMenu = false, purgeModifiers = false) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.post('menu/purge', { key, store_id, purgeMenu, purgeModifiers });
    return response;
  }

  async loadMenu(key, store_id, items, modifiers) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.post('menu/loadMenu', { key, store_id, items, modifiers });
    return response;
  }

  async loadModifiers(key, store_id, modifiers) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.post('menu/loadModifiers', { key, store_id, modifiers });
    return response;
  }

  async getModifiers(key, store_id, select, limit) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.get('menu/modifiers', { params: { key, store_id, select, limit } });
    return response;
  }

  async loadBarcodes(key, firm_id, store_id, barcodes, mode='test') {
    if (!key || !(firm_id || store_id))
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.post('menu/loadBarcodes', { key, firm_id, store_id, barcodes, mode });
    return response;
  }

  async getBarcodes(key, store_id, select, limit) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.get('menu/barcodes', { params: { key, store_id, select, limit } });
    return response;
  }

  async loadCustomers(key, store_id, customers) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.post('menu/loadCustomers', { key, store_id, customers });
    return response;
  }

  async loadCustomerProps(key, store_id, customers) {
    if (!key || !store_id)
      throw new Error('Key or Store ID is missing');

    const response = await this.enlite.request.post('menu/loadCustomerProps', { key, store_id, customers });
    return response;
  }

  async getCustomers(key, firm_id, store_id, select, limit) {
    if (!key || !(firm_id || store_id))
      throw new Error('Key, Firm ID or Store ID is missing');

    const response = await this.enlite.request.get('menu/customers', { params: { key, firm_id, store_id, select, limit } });
    return response;
  }

}