//
module.exports = class Service {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async print(station_code, store_id, body) {
    if (!station_code || !store_id)
      throw new Error('Store ID and Invoice ID are required');

    const response = await this.enlite.request.post(`service/print`, {...body, station_code, store_id});
    return response;
  }

  //
  async directions(query) {
    if (!query)
      throw new Error('Query field is empty');

    const response = await this.enlite.request.post(`service/directions`, query);
    return response;
  }

  //
  async geocode(query) {
    if (!query)
      throw new Error('Query field is empty');

    const response = await this.enlite.request.post(`service/geocode`, query);
    return response;
  }

  //
  async markers(google_sheet, sheet_name) {
    if (!google_sheet || !sheet_name)
      throw new Error('Google Sheet and Sheet Name are required');

    const response = await this.enlite.request.post(`service/markers`, {google_sheet, sheet_name});
    return response;
  }

}