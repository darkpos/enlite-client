//
module.exports = class Customer {

  //
  constructor(enlite) {
    this.enlite = enlite
    return this
  }

  //
  async searchCustomer(store_id, search) {
    if (!search || !store_id)
      throw new Error('Search or Store ID is missing');

    const response = await this.enlite.request.get(`customer/?store_id=${store_id}&search=${search}`);
    return response;
  }

  //
  async selectCustomer(params={}) {
    if (!Object.values(params).length)
      throw new Error('Params is missing');

    const response = await this.enlite.request.get(`customer/select`, { params });
    return response;
  }

  //
  async createCustomer(store_id, customer, created_employee_id) {
    if (!customer || !store_id)
      throw new Error('Customer or Store ID is missing');

    customer.created_employee_id = created_employee_id;
    const response = await this.enlite.request.post(`customer`, { store_id, customer });
    return response;
  }

  //
  async createPublicCustomer(customer) {
    if (!customer)
      throw new Error('Customer is missing');

    const response = await this.enlite.request.post(`customer/public`, { customer });
    return response;
  }

  //
  async getCustomer(store_id, customer_id) {
    if (!customer_id || !store_id)
      throw new Error('Customer ID or Store ID is missing');

    const response = await this.enlite.request.get(`customer/${store_id}/${customer_id}`);
    return response;
  }

  //
  async getCustomerBase(store_id, customer_id) {
    if (!customer_id || !store_id)
      throw new Error('Customer ID or Store ID is missing');

    const response = await this.enlite.request.get(`customer/${store_id}/${customer_id}/base`);
    return response;
  }

  //
  async getCustomerInvoices(store_id, customer_id) {
    if (!customer_id || !store_id)
      throw new Error('Customer ID or Store ID is missing');

    const response = await this.enlite.request.get(`customer/${store_id}/${customer_id}/invoices`);
    return response;
  }

  //
  async updateCustomer(store_id, customer_id, customer, updated_employee_id) {
    if (!customer_id || !store_id)
      throw new Error('Customer ID or Store ID is missing');

    customer.updated_employee_id = updated_employee_id;
    const response = await this.enlite.request.put(`customer/${customer_id}`, { store_id, customer });
    return response;
  }

  //
  async updateCustomerCoordinate(store_id, customer_id, customer, updated_employee_id) {
    if (!customer_id || !store_id)
      throw new Error('Customer ID or Store ID is missing');

    customer.updated_employee_id = updated_employee_id;
    const response = await this.enlite.request.put(`customer/geocode/${customer_id}`, { store_id, customer });
    return response;
  }

  //
  async updateCustomerCoordinates(store_id, customers, updated_employee_id) {
    if (!store_id || !Array.isArray(customers))
      throw new Error('Store ID or Customers is missing');

    const customersUpdated = customers.map(customer => {
      return {
        ...customer,
        updated_employee_id
      }
    })
    
    const response = await this.enlite.request.put(`customer/geocode`, { store_id, customers: customersUpdated });
    return response;
  }

  //
  async updateCustomerDetail(store_id, detail_id, detail, updated_employee_id) {
    if (!detail_id || !store_id)
      throw new Error('Detail ID or Store ID is missing');

    detail.updated_employee_id = updated_employee_id;
    const response = await this.enlite.request.put(`customer/detail/${detail_id}`, { store_id, detail });
    return response;
  }


  //
  async getDeliverySlots(firm_id, store_id, customer_id, from_date, to_date, route_slots) {
    if (!firm_id || !store_id || !customer_id)
      throw new Error('Firm ID, Store ID and Customer ID are required');

    const response = await this.enlite.request.get(`customer/deliverySlots/${firm_id}/${store_id}/${customer_id}`, { params: { from_date, to_date, route_slots } });
    return response;
  }

  //
  async createCustomerCardId(store_id, customer_id) {
    if (!customer_id || !store_id)
      throw new Error('Required fields missing');

    const response = await this.enlite.request({
      method: 'POST',
      url: `customer/cardId`,
      data: { store_id, customer_id }
    });

    return response;
  }

  //
  async createCustomerCreditCard(store_id, customer_id, card_id) {
    if (!customer_id || !store_id || !card_id)
      throw new Error('Required fields missing');

    const response = await this.enlite.request({
      method: 'POST',
      url: `customer/creditCard`,
      data: { store_id, customer_id, card_id }
    });

    return response;
  }

  //
  async removeCustomerCreditCard(store_id, customer_id, card_id) {
    if (!customer_id || !store_id || !card_id)
      throw new Error('Required fields missing');

    const response = await this.enlite.request({
      method: 'DELETE',
      url: `customer/creditCard`,
      data: { store_id, customer_id, card_id }
    });

    return response;
  }

  //
  async addRoute(store_id, customer_id, route_id) {
    if (!customer_id || !store_id || !route_id)
      throw new Error('Required fields missing');

    const response = await this.enlite.request.post(`customer/route`, { 
      store_id,
      customer_id,
      route_id
    });

    return response;
  }

  //
  async removeRoute(store_id, customer_route_id) {
    if (!store_id || !customer_route_id)
      throw new Error('Required fields missing');

    const response = await this.enlite.request.delete(`customer/route`, { data:
      { 
        store_id,
        customer_route_id
      }
    });

    return response;
  }

}