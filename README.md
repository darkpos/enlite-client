# Dark Enlite Client Library

Use this package to access Dark Enlite services.

## Example

```
const Enlite = require('enlite-client');

async function test() {
  const enlite = new Enlite({
    url: `https://enlite-service-broker.darkpos.io`
  })

  // session
  console.log(`>> SESSION >>`);
  const session = await enlite.session.login('employee@domain.com', 'mypassword');
  console.log(`User Id: ${session.data.user.user_id}`);

  // store
  console.log(`\n>> STORE >>`);
  const store = await enlite.store.getStore(session.data.user.firm_id, session.data.user.store_id);
  console.log(`Store Name: ${store.data.store.name}`);

  // customer
  console.log(`\n>> CUSTOMER >>`);
  console.log(`\n>> Create Customer >>`);
  const created = await enlite.customer.createCustomer(session.data.user.store_id, {
    first_name: "Clark",
    last_name: "Kent"
  }, session.data.user.user_id);
  console.log(`Id: ${created.data.id}`);
  console.log(`Name: ${created.data.first_name} ${created.data.last_name}`);

  console.log(`\n>> Update Customer >>`);
  const updated = await enlite.customer.updateCustomer(session.data.user.store_id, created.data.id, {
    last_name: "Kent II"
  }, session.data.user.user_id);
  console.log(`Id: ${updated.data.id}`);

  console.log(`\n>> Get Customer >>`);
  const customer = await enlite.customer.getCustomer(session.data.user.store_id, updated.data.id);
  console.log(`Id: ${customer.data.info.id}`);
  console.log(`Name: ${customer.data.info.first_name} ${customer.data.info.last_name}`);
  console.log(`Tickets: ${customer.data.tickets.length}`);

  console.log(`\n>> Search Customer >>`);
  const customers = await enlite.customer.searchCustomer(session.data.user.store_id, 'kent');
  console.log(`Matches: ${customers.data.length}`);
}

test()
```
